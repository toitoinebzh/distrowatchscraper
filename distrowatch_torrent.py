#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 18:53:47 2018

@author: toitoinebzh
license : GPLv3

This software download every .torrent available on distrowatch.com
Torrents are store in "TORRENT_LOCATION", you can then open them
with transmission and share free software.

enjoy ;)

"""

import os
import urllib
from bs4 import BeautifulSoup


# some "constant"
URL = "https://distrowatch.com/dwres.php?resource=bittorrent&sortorder=date"
WEBSITE = "https://distrowatch.com/"

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
TORRENT_LOCATION = DIR_PATH + "/DistrowatchTorrent/"

# classes
class Torrent:
    "class to represent a torrent"
    def __init__(self, name, torrent_name, torrent_link):
        self.name =  name
        self.torrent_name = torrent_name
        self.torrent_link = WEBSITE + urllib.parse.quote(torrent_link)
    def download(self):
        "to download the torrent"
        if not os.path.exists(TORRENT_LOCATION):
            os.makedirs(TORRENT_LOCATION)
        
        filename = TORRENT_LOCATION + self.torrent_name
        if os.path.isfile(filename) or os.path.isfile(filename + ".added"):
            print("Already in directory : ", self.name)
        else:
            print("Downloading : ", self.name)
            urllib.request.urlretrieve(self.torrent_link, filename)

    def __repr__(self):
        return self.name + " : " + self.torrent_name
    
class DistrowatchPage:
    "class to download on distrowatch page"
    def __init__(self):
        self.url = URL

    def get_torrents(self):
        "to get all the torrent on the page"
        
        html = urllib.request.urlopen(self.url)
        soup = BeautifulSoup(html, "lxml", from_encoding='utf-8')
        main_table = soup.find_all("td",{"class":"torrent"})
        project_col = main_table[0::2]
        torrent_col = main_table[1::2]
        
        torrents = []
        for ind, txt in enumerate(project_col):
            name = project_col[ind].text
            torrent_name = torrent_col[ind].text
            torrent_link = torrent_col[ind].find("a").get("href")
            torrent=Torrent(name, torrent_name, torrent_link)
            #print(torrent)
            #print(torrent.torrent_link)
            torrents.append(torrent)

        return torrents

    def download(self, nb = None):
        """to download all the torrent on the page
           nb to get only the last nb torrent
        """
        
        if nb is None :
            torrents_to_download = self.get_torrents()
        else :
            torrents_to_download = self.get_torrents()[0:nb]
            
        for torrent in torrents_to_download:
            torrent.download()
            
    def torrent_list(self):
        "returns all the available torrent"
        for torrent in self.get_torrents():
            print(torrent)

    def __repr__(self):
        return WEBSITE + " : " + str(len(self.get_torrents())) + " torrents"



# =============================================================================
if __name__ == "__main__":
    print("This program download every torrent on distrowatch.com")
    DP = DistrowatchPage()
    #DP.download(100)
    DP.download()
    